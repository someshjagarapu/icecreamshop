<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Entity\Orders;
use AppBundle\Form\Type\OrdersType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use AppBundle\Constants\FlavourConstants;
use AppBundle\Constants\ToppingConstants;

class OrderController extends Controller {

    /**
     * @Route("/", name="homepage")
     */
    public function homeAction() {
        // render icecream, toppings menu home page
        return $this->render('AppBundle:Order:homepage.html.twig', array(
                    'flavours' => FlavourConstants::FLAVOURS,
                    'toppings' => ToppingConstants::TOPPINGS,
                    'flavourconstants' => FlavourConstants::FLAVOURCOST,
                    'toppingconstants' => ToppingConstants::TOPPINGCOST,
        ));
    }

    /**
     * @Route("/order", name="orderpage")
     */
    public function orderAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        // creates a orders object
        $order = new Orders();
        $form = $this->createForm(OrdersType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // find icecreams
            $icecreams = $order->getIcecreams();
            // call service for duplicate flavours - Commented but can uncomment to test for duplicate flavors
//            $duplicateflavours = $this->container->get('duplicateflavour_service')->getDuplicateFlavour($icecreams);
//            // check duplicate flavours
//            if ($duplicateflavours) {
//                $this->addFlash('error', 'Oops! You have added same flavour more than once');
//                // render same orderpage
//                return $this->render('AppBundle:Order:orderpage.html.twig', array(
//                            'form' => $form->createView(),
//                ));
//            }
            // check icecreams object empty or not
            if ($icecreams->isEmpty()) {
                $this->addFlash('error', 'Please add atleast one icecream');
                return $this->render('AppBundle:Order:orderpage.html.twig', array(
                            'form' => $form->createView(),
                ));
            }
            foreach ($icecreams as $icecream) {
                $icecream->setOrders($order);
                $em->persist($icecream);
            }
            // call service for total order cost
            $totalcost = $this->container->get('totalcost_report_service')->getOrderCost($icecreams, $order);
            $order->setTotalcost($totalcost);
            $em->persist($order);
            $em->flush();
            // redirect to order summarypage with order id
            $url = $this->generateUrl('ordersummarypage', array('id' => $order->getId()));
            $response = new RedirectResponse($url);
            return $response;
        }
        // render orderpage
        return $this->render('AppBundle:Order:orderpage.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/ordersummary/{id}", requirements={"id" = "\d+"}, name="ordersummarypage")
     */
    public function orderSummaryAction($id) {
        $em = $this->getDoctrine()->getManager();
        // order object with orderid
        $order = $em->getRepository('AppBundle:Orders')->findOneBy(array('id' => $id));
        // check wheteher order id is existing or not in database
        if (empty($order)) {
            // render errormessage page
            return $this->render('AppBundle:Order:ordersecuritycheck.html.twig');
        }
        // render ordersummarypage with order object
        return $this->render('AppBundle:Order:ordersummarypage.html.twig', array(
                    'order' => $order,
                    'flavourconstants' => FlavourConstants::FLAVOURS,
                    'toppingconstants' => ToppingConstants::TOPPINGS,
        ));
    }

}
