<?php

namespace AppBundle\Service;

class DuplicateFlavourError {
    /**
     * Returns error message in case of duplicate flavors in the order.
     * 
     * @param array $icecreams
     * @return array 
     */
    public function getDuplicateFlavour($icecreams) {
        // initialize flavourid array
        $flavouridarr = array();
        // iterate icecreams
        foreach ($icecreams as $icecream) {
            // store all flavour ids to flavourid array
            $flavouridarr[] = $icecream->getFlavour();
        }
        // initialize dups array
        $dups = array();
        // find duplicate flavours
        foreach (array_count_values($flavouridarr) as $val => $c) {
            if ($c > 1) {
                $dups[] = $val;
            }
        }
        // return duplicate flavour array
        return $dups;
    }
}