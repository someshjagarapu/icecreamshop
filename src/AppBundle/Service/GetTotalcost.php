<?php

namespace AppBundle\Service;

use AppBundle\Constants\FlavourConstants;
use AppBundle\Constants\ToppingConstants;

class GetTotalcost {

    /**
     * Returns the total cost for the icecream order
     * @param array $icecreams
     * @param Object $order
     * @return number
     */
    public function getOrderCost($icecreams, $order) {
        // initialize icecreamcost, toppingcost, totalcost values
        $icecreamcost = 0;
        $toppingcost = 0;
        $totalcost = 0;
        // iterate icecreams
        foreach ($icecreams as $icecream) {
            // get flavour
            $flavour = $icecream->getFlavour();
            // find each flavourcost using flavourconstants class
            $flavourcost = FlavourConstants::FLAVOURCOST[$flavour];
            // find number of scoops
            $numofscoops = $icecream->getNumofscoops();
            // calculate icecreamcost using flavourcost and number of scoops cost
            $icecreamcost += $flavourcost * $numofscoops;
        }
        // find toppings        
        $toppings = $order->getToppings();
        // iterate toppings
        foreach ($toppings as $topping) {
            // find each toppingcost using topping constants class
            $toppingcost += ToppingConstants::TOPPINGCOST[$topping];
        }
        // find totalcost using icecream cost and topping cost
        $totalcost = $icecreamcost + $toppingcost;
        // return totalcost
        return $totalcost;
    }

}    