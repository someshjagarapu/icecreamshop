<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;  
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Icecream;
use AppBundle\Constants\FlavourConstants;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;


class IcecreamType extends AbstractType{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        //Flavour type and Number of scoops
        $builder->add('flavour', ChoiceType::class, array(
                    'choices'  => FlavourConstants::FLAVOURS,
                    'expanded' => false,
                    'multiple' => false,
                ))
                
                ->add('numofscoops', IntegerType::class, array(
                    'required' => true,
                    'attr' => array('style' => 'width: 55px'),
                ));
    }
    
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => Icecream::class,
            'csrf_protection' => true,
            'validation_groups' => array('placeorder'),
        ));
    }
}