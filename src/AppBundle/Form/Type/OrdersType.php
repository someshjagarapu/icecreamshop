<?php

namespace AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Orders;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use AppBundle\Form\Type\IcecreamType;
use AppBundle\Constants\ToppingConstants;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class OrdersType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        // Customer name 
        $builder->add('customername', TextType::class, array(
            'required' => true,
        ));
        
        // Icecreams
        $builder->add('icecreams', CollectionType::class, array(
            'entry_type' => IcecreamType::class,
            'allow_add' => true,
            'label' => false,
            'required' => true,
        ));

        // Toppings 
        $builder->add('toppings', ChoiceType::class, array(
            'choices' => ToppingConstants::TOPPINGS,
            'expanded' => false,
            'multiple' => true,
        ));

    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'entityManager' => null,
            'data_class' => Orders::class,
            'csrf_protection' => true,
            'validation_groups' => array('placeorder'),
        ));
    }

}