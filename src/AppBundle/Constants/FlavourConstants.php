<?php

namespace AppBundle\Constants;

class FlavourConstants {
    // flavour keys
    const FLAVOUR_VANILLA = 1;
    const FLAVOUR_BUTTERSCOTCH = 2;
    const FLAVOUR_CHOCOLATE = 3;
    
    // flavour costs
    const FLAVOURCOST_VANILLA = 10;
    const FLAVOURCOST_BUTTERSCOTCH = 20;
    const FLAVOURCOST_CHOCOLATE = 30;
    
    // flavours array with flavourname and flavourkey
    const FLAVOURS = array(
        'Vanilla' => self::FLAVOUR_VANILLA,
        'Butterscotch' => self::FLAVOUR_BUTTERSCOTCH,
        'Chocolate' => self::FLAVOUR_CHOCOLATE,
    );
    
    // flavourcost array with flavourkey and flavourcost
    const FLAVOURCOST = array(
        self::FLAVOUR_VANILLA => self::FLAVOURCOST_VANILLA,
        self::FLAVOUR_BUTTERSCOTCH => self::FLAVOURCOST_BUTTERSCOTCH,
        self::FLAVOUR_CHOCOLATE => self::FLAVOURCOST_CHOCOLATE,
    );

} 