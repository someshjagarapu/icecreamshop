<?php

namespace AppBundle\Constants;

class ToppingConstants {
    // topping keys
    const TOPPING_NUTS = 1;
    const TOPPING_CANDY = 2;
    const TOPPING_SPINKLES = 3;
    
    // topping costs
    const TOPPINGCOST_NUTS = 10;
    const TOPPINGCOST_CANDY = 20;
    const FTOPPINGCOST_SPINKLES = 30;
    
    // toppings array with toppingname and toppingkeys
    const TOPPINGS = array(
        'Nuts' => self::TOPPING_NUTS,
        'Candy' => self::TOPPING_CANDY,
        'Spinkles' => self::TOPPING_SPINKLES,
    );
    
    // toppingcost array with toppingkey and topping cost
    const TOPPINGCOST = array(
        self::TOPPING_NUTS => self::TOPPINGCOST_NUTS,
        self::TOPPING_CANDY => self::TOPPINGCOST_CANDY,
        self::TOPPING_SPINKLES => self::FTOPPINGCOST_SPINKLES,
    );
}  