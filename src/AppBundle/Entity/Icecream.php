<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Orders;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Icecream
 *
 * @ORM\Table(name="icecream")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\IcecreamRepository")
 */
class Icecream
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="flavour", type="string", length=255, nullable=true)
     */
    protected $flavour;

    /**
     * @var int
     * @Assert\NotBlank(message="Number of scoops cannot be blank", groups={"placeorder"})
     * @Assert\Range(
     *      min = 1,
     *      minMessage = "You must add at least {{ limit }} scoop to order", groups={"placeorder"}
     * )
     * @ORM\Column(name="numofscoops", type="integer", nullable=true)
     */
    protected $numofscoops;
    
    /**
     * @ORM\ManyToOne(targetEntity="Orders", inversedBy="icecream", cascade={"persist","remove"})
     * @ORM\JoinColumn(name="orders_id", referencedColumnName="id", nullable=true)
     */
    protected $orders;
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set flavour
     *
     * @param string $flavour
     *
     * @return Icecream
     */
    public function setFlavour($flavour)
    {
        $this->flavour = $flavour;

        return $this;
    }

    /**
     * Get flavour
     *
     * @return string
     */
    public function getFlavour()
    {
        return $this->flavour;
    }

    /**
     * Set numofscoops
     *
     * @param integer $numofscoops
     *
     * @return Icecream
     */
    public function setNumofscoops($numofscoops)
    {
        $this->numofscoops = $numofscoops;

        return $this;
    }

    /**
     * Get numofscoops
     *
     * @return integer
     */
    public function getNumofscoops()
    {
        return $this->numofscoops;
    }

    /**
     * Set orders
     *
     * @param \AppBundle\Entity\Orders $orders
     *
     * @return Icecream
     */
    public function setOrders(\AppBundle\Entity\Orders $orders = null)
    {
        $this->orders = $orders;

        return $this;
    }

    /**
     * Get orders
     *
     * @return \AppBundle\Entity\Orders
     */
    public function getOrders()
    {
        return $this->orders;
    }
       
}