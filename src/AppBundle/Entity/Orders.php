<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Icecream;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Orders
 *
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrdersRepository")
 */
class Orders
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var float
     *
     * @ORM\Column(name="totalcost", type="float", nullable=true)
     */
    protected $totalcost;

    /**
     * @var string
     * @Assert\NotBlank(message="Customername cannot be blank", groups={"placeorder"})
     * @Assert\Length(min=2,max=100,minMessage="Customername should be minimum {{ limit }} characters", maxMessage="Your Customername cannot be longer than {{ limit }} characters",groups={"placeorder"}
     * )
     * @ORM\Column(name="customername", type="string", length=255, nullable=true)
     */
    protected $customername;
    
    /**
     * @Assert\Valid
     * @ORM\OneToMany(targetEntity="Icecream", mappedBy="orders", cascade={"persist"})
     */
    protected $icecreams;
    
    
    /**
     * @var array
     *
     * @ORM\Column(name="toppings", type="array", nullable=true)
     */
    protected $toppings;
    
    
    public function __construct() {
        $this->icecreams = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set totalcost
     *
     * @param float $totalcost
     *
     * @return Orders
     */
    public function setTotalcost($totalcost)
    {
        $this->totalcost = $totalcost;

        return $this;
    }

    /**
     * Get totalcost
     *
     * @return float
     */
    public function getTotalcost()
    {
        return $this->totalcost;
    }

    /**
     * Set customername
     *
     * @param string $customername
     *
     * @return Orders
     */
    public function setCustomername($customername)
    {
        $this->customername = $customername;

        return $this;
    }

    /**
     * Get customername
     *
     * @return string
     */
    public function getCustomername()
    {
        return $this->customername;
    }

    /**
     * Set toppings
     *
     * @param array $toppings
     *
     * @return Orders
     */
    public function setToppings($toppings)
    {
        $this->toppings = $toppings;

        return $this;
    }

    /**
     * Get toppings
     *
     * @return array
     */
    public function getToppings()
    {
        return $this->toppings;
    }

    /**
     * Add icecream
     *
     * @param \AppBundle\Entity\Icecream $icecream
     *
     * @return Orders
     */
    public function addIcecream(\AppBundle\Entity\Icecream $icecream)
    {
        $this->icecreams[] = $icecream;

        return $this;
    }

    /**
     * Remove icecream
     *
     * @param \AppBundle\Entity\Icecream $icecream
     */
    public function removeIcecream(\AppBundle\Entity\Icecream $icecream)
    {
        $this->icecreams->removeElement($icecream);
    }

    /**
     * Get icecreams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getIcecreams()
    {
        return $this->icecreams;
    }
}