jQuery(document).ready(function() {
    
    // Get the ul that holds the collection of icecream
    $collectionHolder = $('ul.icecream');
    
    for (var i = 0; i < $collectionHolder.find(":input").length; i++) {

        $("#orders_icecreams_" + i + "_flavour").chosen({
            display_selected_options: false,
            width: "100%",
            placeholder_text_multiple: 'Select Icecream',
            no_results_text: "Oops, no Icecream found!"
        });
    }    

});