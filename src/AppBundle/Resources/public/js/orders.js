var $collectionHolder;

// setup an "add a Icecream" link
var $addTagButton = $('<a href="#" class="add_tag_link btn btn-primary a-btn-slide-text" style="margin-top:15px;">\n\
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>\n\
                            <span><strong>Add an Icecream</strong></span></a>');
var $newLinkLi = $('<li></li>').append($addTagButton);

jQuery(document).ready(function() {
    
    // Get the ul that holds the collection of icecream
    $collectionHolder = $('ul.icecream');
    
    $("#orders_toppings").chosen({
        display_selected_options: false,
        width: "100%",
        placeholder_text_multiple: 'Select Toppings',
        no_results_text: "Oops, no toppings found!"
    });        
    
    // add the "add a icecream" anchor and li to the icecream ul
    $collectionHolder.append($newLinkLi);

    // count the current form inputs we have (e.g. 2), use that as the new
    // index when inserting a new item (e.g. 2)
    $collectionHolder.data('index', $collectionHolder.find(':input').length);

    $addTagButton.on('click', function(e) {
        // prevent the link from creating a "#" on the URL
        e.preventDefault();
        // add a new icecream form (see next code block)
        addTagForm($collectionHolder, $newLinkLi);
    });
});

function addTagForm($collectionHolder, $newLinkLi) {
    // Get the data-prototype explained earlier
    var prototype = $collectionHolder.data('prototype');

    // get the new index
    var index = $collectionHolder.data('index');

    var newForm = prototype;
    // Replace '__name__label__' in the prototype's HTML to
    // instead be a number based on how many items we have
    // newForm = newForm.replace(/__name__label__/g, index);

    // Replace '__name__' in the prototype's HTML to
    // instead be a number based on how many items we have
    newForm = newForm.replace(/__name__/g, index);

    // increase the index with one for the next item
    $collectionHolder.data('index', index + 1);

    // Display the form in the page in an li, before the "Add a icecream" link li
    var $newFormLi = $('<li></li>').append(newForm);
    $newLinkLi.before($newFormLi);
    
}