<?php

namespace Tests\AppBundle\Service;

use AppBundle\Entity\Orders;
use AppBundle\Entity\Icecream;
use AppBundle\Constants\FlavourConstants;
use AppBundle\Constants\ToppingConstants;
use AppBundle\Service\GetTotalcost;
use PHPUnit\Framework\TestCase;

class GetTotalcostTest extends TestCase {

    protected $testtotalcost;

    public function testOrderCostOne() {
        //CASE 1 : 3 ICECREAMS AND 3 TOPPINGS

        $this->testtotalcost = new GetTotalcost();
        // create new orders object
        $order = new Orders();
        // create new icecream boject
        $icecream1 = new Icecream();
        // set flavour to icecream1 object
        $icecream1->setFlavour(FlavourConstants::FLAVOURS['Vanilla']);
        //set number of scoops to icecream1 object
        $icecream1->setNumofscoops(1);
        $order->addIcecream($icecream1);
        $icecream1->setOrders($order);

        // create new icecream object and add flavour and numofscoops    
        $icecream2 = new Icecream();
        $icecream2->setFlavour(FlavourConstants::FLAVOURS['Butterscotch']);
        $icecream2->setNumofscoops(1);
        $order->addIcecream($icecream2);
        $icecream2->setOrders($order);

        // create new icecream object and add flavour and numofscoops
        $icecream3 = new Icecream();
        $icecream3->setFlavour(FlavourConstants::FLAVOURS['Chocolate']);
        $icecream3->setNumofscoops(1);
        $order->addIcecream($icecream3);
        $icecream3->setOrders($order);

        // set toopings array
        $toppings = array(ToppingConstants::TOPPINGS['Nuts'],
            ToppingConstants::TOPPINGS['Candy'],
            ToppingConstants::TOPPINGS['Spinkles']);
        // set toppings to order object
        $order->setToppings($toppings);

        // call service for totalcost
        $gettotalcost = $this->testtotalcost->getOrderCost($order->getIcecreams(), $order);
        // set totalcost to order object
        $order->setTotalcost($gettotalcost);

        // assert that your totalcost added correctly!
        $this->assertEquals(120, $order->getTotalcost());
    }

    public function testOrderCostTwo() {
        //CASE 2 : 2 ICECREAMS AND  0 TOPPINGS      

        $this->testtotalcost = new GetTotalcost();
        // create new orders object
        $order = new Orders();
        // create new icecream boject
        $icecream1 = new Icecream();
        // set flavour to icecream1 object
        $icecream1->setFlavour(FlavourConstants::FLAVOURS['Vanilla']);
        //set number of scoops to icecream1 object
        $icecream1->setNumofscoops(1);
        $order->addIcecream($icecream1);
        $icecream1->setOrders($order);

        // create new icecream object and add flavour and numofscoops    
        $icecream2 = new Icecream();
        $icecream2->setFlavour(FlavourConstants::FLAVOURS['Butterscotch']);
        $icecream2->setNumofscoops(1);
        $order->addIcecream($icecream2);
        $icecream2->setOrders($order);


        // set toopings array
        $toppings = array();
        // set toppings to order object
        $order->setToppings($toppings);

        // call service for totalcost
        $gettotalcost = $this->testtotalcost->getOrderCost($order->getIcecreams(), $order);
        // set totalcost to order object
        $order->setTotalcost($gettotalcost);

        // assert that your totalcost added correctly!
        $this->assertEquals(30, $order->getTotalcost());
    }

}
